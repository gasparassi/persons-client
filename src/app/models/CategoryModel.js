/**
 * Category Model
 */

export default class CategoryModel {

  id = null;
  name = "";

  constructor(name) {
    this.name = name;
  }
}
