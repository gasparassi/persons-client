/**
 * Person Model
 */

export default class PersonModel {

  id = null;
  category_id = null;
  name = "";
  email = "";

  constructor(category_id, name, email) {
    this.category_id = category_id;
    this.name = name;
    this.email = email;
  }

}
