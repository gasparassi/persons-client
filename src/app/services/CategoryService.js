import Rest from './Rest'

/**
 * @typedef {CategoryService}
 */
export default class CategoryService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/categories'

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
  }
}
