import Rest from './Rest'

/**
 * @typedef {CategorySelectService}
 */
export default class CategorySelectService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/categories/select'
}
