import Api from '../services/API';

/**
 * @typedef {Rest}
 */
export default class Rest extends Api {
	/**
	 * @type {String}
	 */
	static resource = '';

	/**
	 * @type {String}
	 */
	id = 'id';

	/**
	 * @param {String} resource
	 * @param {Object} options
	 * @param {Object} http
	 */
	constructor(resource, options = {}, http = null) {
		super(Rest.normalize(Rest.base, resource), options, http);
	}

	/**
	 * @return {this}
	 */
	static build() {
		return new this(this.resource);
	}

	/**
	 * @param {Object} record
	 * @returns {*|PromiseLike<T | never>|Promise<T | never>}
	 */
	create(record) {
		return this.post('', record)
			.then((response) => {
				return response;
			})
			.catch((error) => {
				return this.errors(error);
			});
	}

	/**
	 * @param {String|Object} record
	 * @returns {*|PromiseLike<T | never>|Promise<T | never>}
	 */
	read(record) {
		return this.get(`/${this.getId(record)}`).catch((error) => {
			return this.errors(error);
		});
	}
	/**
	 * @param {Object} record
	 * @returns {*|PromiseLike<T | never>|Promise<T | never>}
	 */
	update(record) {
		return this.put(`/${this.getId(record)}`, record).catch((error) => {
			return this.errors(error);
		});
	}

	/**
	 * @param {Object} record
	 * @returns {*|PromiseLike<T | never>|Promise<T | never>}
	 */
	destroy(record) {
		return this.delete(`/${this.getId(record)}`).catch((error) => {
			return this.errors(error);
		});
	}

	/**
	 * @param {Object} parameters
	 * @returns {*|PromiseLike<T | never>|Promise<T | never>}
	 */
	search(parameters = {}) {
		const queryString = parameters !== null ? parameters : '';
		return this.get(`?${queryString}`)
			.then((response) => {
				return response;
			})
			.catch((error) => {
				return this.errors(error);
			});
	}

	/**
	 * @param {Object} response
	 * @returns {Object}
	 */
	errors(response) {
		if (typeof response.data === 'string') {
			let data = JSON.parse(response.data);
			return {
				errors: data,
				code: data.status
			};
		}
	}

	/**
	 * @param {String|Object} record
	 * @returns {String}
	 */
	getId(record) {
		if (typeof record === 'object') {
			return record[this.id];
		}
		return String(record);
	}
}
