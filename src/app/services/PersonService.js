import Rest from './Rest'

/**
 * @typedef {PersonService}
 */
export default class PersonService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/persons';

    rules = {
      required: (value) => !!value || "Este campo é obrigatório",
      emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
    }
}
