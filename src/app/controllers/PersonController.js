import PersonService from '../services/PersonService';
import PersonModel from '../models/PersonModel';

/**
 * @typedef {PersonController}
 */
export default class PersonController {

    /**
    * @type {null}
     */

    service = null;

    constructor() {
        this.service = PersonService.build();
    }

    personModel() {
        return new PersonModel();
    }

    register(person) {
        return this.service.create(person).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response;
            }
        );
    }

    update(person) {
        return this.service.update(person).then(
            response => {
                return response;
            }
        );
    }

    delete(person) {
        return this.service.destroy(person).then(
            response => {
                return response;
            }
        );
    }

}
