import CategoryService from '../services/CategoryService';
import CategorySelectService from '../services/CategorySelectService';
import CategoryModel from '../models/CategoryModel';

/**
 * @typedef {CategoryController}
 */
export default class CategoryController {

    /**
    * @type {null}
     */

    service = null;
    serviceSelect = null;

    constructor() {
        this.service = CategoryService.build();
        this.serviceSelect = CategorySelectService.build();
    }

    categoryModel() {
        return new CategoryModel();
    }

    register(category) {
        return this.service.create(category).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response;
            }
        );
    }

    getAllForSelect() {
        return this.serviceSelect.search(null).then(
            response => {
                return response;
            }
        );
    }

    update(category) {
        return this.service.update(category).then(
            response => {
                return response;
            }
        );
    }

    delete(category) {
        return this.service.destroy(category).then(
            response => {
                return response;
            }
        );
    }

}
