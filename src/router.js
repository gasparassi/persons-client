import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},
				{
					name: 'category-form',
					path: 'pages/categories/register',
					component: () => import('@/views/pages/categories/CategoryForm'),
				},
				{
					name: 'categories',
					path: 'pages/categories/index',
					component: () => import('@/views/pages/categories/Categories'),
				},
				{
					name: 'person-form',
					path: 'pages/person/register',
					component: () => import('@/views/pages/person/PersonForm'),
				},
				{
					name: 'persons',
					path: 'pages/person/index',
					component: () => import('@/views/pages/person/Persons'),
				},
			]
		}
	]
});

export default router;
