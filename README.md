# [Vue Light Bootstrap Dashboard](http://vuejs.creative-tim.com/vue-light-bootstrap-dashboard) [![version][version-badge]][CHANGELOG] [![license][license-badge]][LICENSE]

> Admin dashboard based on light bootstrap dashboard UI template + vue-router

Esse template foi obtido gratuitamente em [Light bootstrap dashboard]
(https://www.creative-tim.com/product/light-bootstrap-dashboard)
projetado para vue js.

Para visualizar on-line, clique aqui 
[Live Demo here](http://vuejs.creative-tim.com/vue-light-bootstrap-dashboard).

### Esse projeto deverá ser executado consumindo a API disponível para download no link abaixo:
(https://gitlab.com/gasparassi/persons-client)

## Esse client web só poderá ser executatando em perfeito funcionamento se a API disponível no link https://gitlab.com/gasparassi/persons-client estiver on-line. 

## :rocket: Começando

Vue Light Bootstrap Dashboard é construído em cima do Bootstrap 4,
Vuejs e Vue-router. Para começar, execute as seguintes etapas:
1. Faça o download do projeto
2. Tenha certeza que você possua o node.js (https://nodejs.org/en/) instalado em seu computador
3. Execute `npm install` na pasta de origem onde o arquivo `package.json` está localizado
4. Execute `npm run dev` para iniciar o servidor de desenvolvimento

## [Documentação](https://demos.creative-tim.com/vue-light-bootstrap-dashboard/documentation/#/buttons)

## :cloud: Configuração de compilação

### instalar dependências
`npm install`
### o servidor de aplicação(api) deverá estar executando em localhost:8000
`npm run dev`


Para obter uma explicação detalhada sobre como as coisas funcionam, verifique o [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

[CHANGELOG]: ./CHANGELOG.md
[LICENSE]: ./LICENSE.md
[version-badge]: https://img.shields.io/badge/version-1.0.0-blue.svg
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
